# monitor
## 背景
怀疑服务器有性能问题！？用profile吗？ profile并不能看出所有问题，只能看出大量调用后的平均消耗。还有就是真正的运行环境中不一定让你使用profile。

如果出现单次消耗时间过长，又不知道是哪个函数消耗时间过长。或者想知道某些函数长时间的调用次数，平均时间。怎么办？

例如：创建任务接口 给1个终端发任务，和给 100W终端发任务消耗时间肯定不一样。那我想知道时间消耗在哪里，调用链上面每个函数的消耗时间是否合理？针对那个调用函数需要注意？

## 解决思路
提供一个监控框架，实现监控功能，打印部分函数的单次消耗时间，以及间隔一段时间进行数据采集。

### 监控提供两个功能
#### 函数信息
单个函数的消耗时间，在需要采集数据的函数中添加采集代码，进行采集信息，函数执行完成后进行回调。

#### 周期信息
间隔一段时间进行数据采集，根据单个函数的数据采集，整理，并且以间隔时间作为周期进行定期输出信息。

定时输出信息，每隔一个周期时间输出一次，平均调用时间，以及调用次数。

### 监控规则
添加加监控级别，如果只想控制周期任务，或者只想看函数信息那么需要设置对应的显示规则。

## 实现

### 监控级别设置
启动传参设置不同监控级别，方便控制输出信息，和统计信息。

enum|描述
|---|---|
Close|关闭监控，不做任何采集，以及输出
Func|开启监控，输出主要信息（调用时间，和调用方信息）
Periodic|开启监控，每分钟输出平均调用时间，以及调用次数
All|开启监控，打印所有信息


### 输出方式：
提供给使用方数据打印信息HOOK，由使用方决定使用输出方式。
```
func OutStringHook(s string) error{
}
```




